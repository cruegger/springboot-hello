// @Library('github.com/releaseworks/jenkinslib') _
pipeline {
    agent any
    triggers {
       cron('15 3 * * * ')
    }
    environment {
        JOB_NAME = "Build Jenkins Pipeline for springboot-hello"
        KUBE_CONFIG = "/var/lib/jenkins/kube-config"
    }
    stages {

        stage ('Checkout') {
            steps {
                echo "Checking out Repository"
                cleanWs()
                checkout scm

            }
        }

       stage ('Build') {
            steps {
                buildModule("hello")
            }
        }
        stage('Run Tests') {
          steps {
            runTests()
          }
        }

        stage('Publish') {
          steps {
            publishImage("hello")
          }
        }
        stage ('Deploy') {
          steps {
             deployApp()
          }
        }
    }
}

def buildModule(moduleName) {
    echo 'Build ${moduleName}'
    sh 'pwd'
    sh 'ls -l'
    sh 'mvn clean package -Dmaven.test.skip=true'
}

def runTests() {
  echo 'Run Tests'
  sh 'mvn test'
}

def deployApp() {
  echo 'deployApp() BEGIN'
  if (env.BRANCH_NAME == 'master') {
    echo 'deploying from MASTER branch: ' + env.BRANCH_NAME
    withKubeConfig([credentialsId: 'kube-config']) {
      sh 'helm template ./deploy --values ./deploy/values-prod.yml | kubectl apply -f -'
    }
  } else if (env.BRANCH_NAME == 'develop') {
    echo 'deploying from DEVELOP branch: ' + env.BRANCH_NAME
    withKubeConfig([credentialsId: 'kube-config']) {
      sh 'helm template ./deploy --values ./deploy/values-intg.yml | kubectl apply -f -'
    }
  } else if (env.BRANCH_NAME.contains("feature")) {
    echo 'deploying from FEATURE branch: ' + env.BRANCH_NAME
    withKubeConfig([credentialsId: 'kube-config']) {
      sh 'helm template ./deploy --values ./deploy/values-dev.yml | kubectl apply -f -'
    }
  }
}


def publishImage(moduleName) {
    echo "Publish ${moduleName} Image to Dockerhub"
    def arch = sh(script: 'uname -m | tr -d [:space:]', returnStdout: true)
    def appName = moduleName.toLowerCase()
    echo "appName= ${appName}"
    def pom = readMavenPom file: 'pom.xml'
    echo "${moduleName} Version=${pom.version}"
    def appVersion = pom.version
    sh ("docker image prune -f")
    def dockerImageWithTagDockerHub = "rueggerc/springboot-${appName}-${arch}:${appVersion}"
    echo "dockerImageWithTagDockerHub=${dockerImageWithTagDockerHub}"
    echo "Building Docker Image BEGIN"
    dockerImage = docker.build("${dockerImageWithTagDockerHub}")
    echo "Building Docker Image END"
    docker.withRegistry('','docker-hub-credentials') {
      dockerImage.push("${appVersion}")
      dockerImage.push("latest");
    }
    sh ("docker rmi ${dockerImageWithTagDockerHub}")
}