package com.rueggerllc.hello.controller;

import com.rueggerllc.hello.model.HelloBean;
import com.rueggerllc.hello.service.HelloService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(com.rueggerllc.hello.controller.HelloController.class)
@Slf4j
public class ControllerTest {

  @Autowired private MockMvc mvc;
  @MockBean private HelloService helloService;

  @Test
  // @Ignore
  public void testSayHello() throws Exception {

    when(helloService.execute(any())).thenReturn(getDummyHelloBean());

    MvcResult mvcResult =
        mvc.perform(get("/hello/v1/execute").contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn();
    assertNotNull(mvcResult);
    assertNotNull(mvcResult.getResponse());
    assertEquals(HttpStatus.OK.value(), mvcResult.getResponse().getStatus());
    String result = mvcResult.getResponse().getContentAsString();
    System.out.println("RESULT=" + result);
    System.out.println("Controller Test Completed");
  }

  public HelloBean getDummyHelloBean() {
    HelloBean helloBean = new HelloBean();
    helloBean.setMessage("Hello From Dummy Hello Bean");
    return helloBean;
  }
}
