package com.rueggerllc.hello.service;

import com.rueggerllc.hello.model.FredBean;
import com.rueggerllc.hello.model.HelloBean;
import com.rueggerllc.hello.model.InfoBean;
import com.rueggerllc.hello.util.HelloServiceHelper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {HelloServiceImpl.class})
@Slf4j
public class ServiceTest {

  @MockBean private HelloServiceHelper helloServiceHelper;

  @MockBean private FredBean fredBean;

  @MockBean private InfoBean infoBean;

  @InjectMocks @Autowired HelloServiceImpl helloService;

  @Rule
  public TestWatcher watchman = new TestWatcher() {

    @Override
    protected void failed(Throwable e, Description description) {
      log.info("Test FAILED");
    }

    @Override
    protected void succeeded(Description description) {
      log.info("TEST SUCCEEDED");
      log.info("TestClass=" + description.getTestClass());
      log.info("MethodName=" + description.getMethodName());
    }
  };

  @Test
  public void testHelloService() {
    when(helloServiceHelper.generateURL(any())).thenReturn("http://localhost/foobar");
    when(fredBean.execute(any())).thenReturn("Hello From Fred Bean");
    HelloBean helloBean = helloService.execute("Larry:");
    System.out.println("HelloBean=" + helloBean);
    Assert.assertNotNull(helloBean);
  }

  @Test
  public void testHelloServiceMockHelper() {
    when(helloServiceHelper.generateURL(any())).thenReturn(mockHelper("Barney"));
    when(fredBean.execute(any())).thenReturn("Hello From Fred Bean");

    HelloBean helloBean = helloService.execute("Larry:");
    System.out.println("HelloBean=" + helloBean);
    Assert.assertNotNull(helloBean);
  }

  private String mockHelper(String parm) {
    log.info("MOCK Helper Code {}", parm);
    return "http://localhost/tampa";
  }
}
