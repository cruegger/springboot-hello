package com.rueggerllc.hello.util;

import org.springframework.stereotype.Component;

@Component
public class HelloServiceHelper {

  public String generateURL(String parm) {
    return String.format("http://localhost/%s", parm);
  }
}
