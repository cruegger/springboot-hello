package com.rueggerllc.hello.controller;

import com.rueggerllc.hello.model.HealthCheckResponse;
import com.rueggerllc.hello.service.HealthCheckService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/hello/v1")
public class HealthCheckController {

  @Autowired
  HealthCheckService healthCheckService;

  @GetMapping(path = "", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<HealthCheckResponse> healthCheck() {
    log.info("healthCheck() BEGIN version=1.3.2");
    HealthCheckResponse response = healthCheckService.execute();
    return new ResponseEntity<>(response, HttpStatus.OK);
  }

}
