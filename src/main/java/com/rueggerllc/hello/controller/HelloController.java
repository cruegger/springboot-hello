package com.rueggerllc.hello.controller;

import com.rueggerllc.hello.model.*;
import com.rueggerllc.hello.service.HelloService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/hello/v1")
public class HelloController {

  @Autowired HelloService helloService;
  @Autowired Environment environment;

  @GetMapping(path = "/execute", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<HelloBean> hello() {
    System.out.println("======== HelloController.hello() BEGIN");
    log.info("sayHello() BEGIN");
    log.info("Executing Service");
    dumpEnvironment();
    HelloBean helloBean = helloService.execute("Barney");
    return new ResponseEntity<>(helloBean, HttpStatus.OK);
  }

  @GetMapping(path = "/sayhello/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<HelloBean> sayHelloUserName(@PathVariable String username) {
    log.info("sayHello() {} BEGIN", username);
    dumpEnvironment();
    log.info("Executing Service");
    HelloBean helloBean = helloService.execute(username);
    return new ResponseEntity<>(helloBean, HttpStatus.OK);
  }

  @GetMapping(path = "/nfs/inq", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<AccessNFSResponse> nfsInq() {
    log.info("nfsInq() {} BEGIN");
    AccessNFSResponse response = helloService.nfsInq();
    return new ResponseEntity<>(response, HttpStatus.OK);
  }

  @GetMapping(path = "/cm/inq", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<AccessConfigMapResponse> cmInq() {
    log.info("cmInq() {} BEGIN");
    AccessConfigMapResponse response = helloService.configMapInq();
    return new ResponseEntity<>(response, HttpStatus.OK);
  }



  @PostMapping(
      path = "/setData",
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<HelloResponse> setData(@RequestBody HelloRequest request) {
    log.info("setData() BEGIN");
    log.info("Request={}", request);
    dumpEnvironment();
    HelloResponse response = helloService.setData(request);
    return new ResponseEntity<>(response, HttpStatus.OK);
  }

  private void dumpEnvironment() {
    List<String> profiles = Arrays.asList(environment.getActiveProfiles());
    for (String profile : profiles) {
      log.info("Next Profile=" + profile);
    }
  }
}
