package com.rueggerllc.hello.service;

import com.rueggerllc.hello.model.*;

public interface HelloService {
  HelloBean execute(String parm);
  HelloResponse setData(HelloRequest request);

  AccessNFSResponse nfsInq();

  AccessConfigMapResponse configMapInq();

  String getVersion();

}
