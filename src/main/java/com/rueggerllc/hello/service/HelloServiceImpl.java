package com.rueggerllc.hello.service;

import com.rueggerllc.hello.model.*;
import com.rueggerllc.hello.util.HelloServiceHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;

@Service
@Slf4j
public class HelloServiceImpl implements HelloService {

  @Autowired private Environment environment;

  @Autowired private HelloServiceHelper helper;

  @Autowired FredBean fredBean;

  @Autowired InfoBean infoBean;

  @Value("${appconfig.name}")
  private String applicationName;

  @Value("${appconfig.version}")
  private String buildVersion;

  @Value("${appconfig.timestamp}")
  private String buildTimestamp;

  @Override
  public String getVersion() {
    return buildVersion;
  }

  @Override
  public HelloBean execute(String parm) {
    log.info("==== Hello Service execute with parm={}", parm);
    log.info("application.name={}", applicationName);
    log.info("build.version={}", buildVersion);
    log.info("maven.build.timestamp={}", buildTimestamp);
    String[] profiles = environment.getActiveProfiles();
    log.info("profiles={}", profiles);
    String environment = String.join(",", profiles);

    HelloBean helloBean = new HelloBean();
    helloBean.setVersion(buildVersion);
    helloBean.setApplicationName(applicationName);
    helloBean.setBuildTimestamp(buildTimestamp);
    helloBean.setEnvironments(environment);
    helloBean.setNewField("New Field Here!");
    helloBean.setHostName(getHostName());
    helloBean.setDbServer(infoBean.getDbServer());

    helloBean.setMessage("Hello " + parm);
    log.info("URL FROM HELPER=" + helper.generateURL(parm));
    log.info("Output from Fred Bean={}", fredBean.execute(parm));

    return helloBean;
  }

  @Override
  public AccessNFSResponse nfsInq() {
    log.info("==== Hello Service nfsInq() BEGIN");
    return accessNFSVolume();
  }

  @Override
  public AccessConfigMapResponse configMapInq() {

    AccessConfigMapResponse response = new AccessConfigMapResponse();
    response.setInfoBean(infoBean);
    return response;
  }

  public HelloResponse setData(HelloRequest request) {
    HelloResponse response = new HelloResponse();
    response.setMessage("Response from Hello.setData()");
    response.setVersion("3.1.1");
    response.setDbServer(infoBean.getDbServer());
    return response;
  }

  private String getHostName() {
    try {
      return InetAddress.getLocalHost().getHostName();
    } catch (Exception e) {
      return "ErrorGettingHostName";
    }
  }


  private AccessNFSResponse accessNFSVolume() {
    log.info("=========== Access NFS VOLUME BEGIN =====");
    log.info("NFS is mounted at /var/nfs by Manifest");
    AccessNFSResponse response = new AccessNFSResponse();
    response.setMsg("NFS is mounted at /var/nfs by Manifest");

    StringBuilder buffer = new StringBuilder();
    try {
      String fileName = "/var/nfs/data.txt";
      File file = new File(fileName);
      if (file.exists()) {
        response.setCode(200);
        log.info("FILE /var/nfs/data.txt FOUND");
        FileInputStream fileInputStream = new FileInputStream(fileName);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
        String line = null;
        while (((line = bufferedReader.readLine()) != null)) {
          log.info("Line={}", line);
          response.getLines().add(line);
        }
      } else {
        response.setCode(404);
        log.info("FILE /var/nfs/data.txt NOT FOUND");
      }

    } catch (Exception e) {
      log.error("ERROR Accessing VOLUME: {}", e);
    }
    return response;
  }
}
