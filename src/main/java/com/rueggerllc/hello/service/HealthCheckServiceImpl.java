package com.rueggerllc.hello.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;import org.springframework.stereotype.Service;
import com.rueggerllc.hello.model.HealthCheckResponse;

@Service
@Slf4j
public class HealthCheckServiceImpl implements HealthCheckService {

  @Autowired private Environment environment;

  @Value("${appconfig.name}")
  private String applicationName;

  @Value("${appconfig.version}")
  private String buildVersion;

  @Value("${appconfig.timestamp}")
  private String buildTimestamp;

  @Value("${appconfig.message}")
  private String message;

  @Override
  public HealthCheckResponse execute() {
    HealthCheckResponse response = new HealthCheckResponse();
    response.setApplicationName(applicationName);
    response.setBuildTimestamp(buildTimestamp);
    response.setVersion(buildVersion);
    response.setMessage(message);

    String[] profiles = environment.getActiveProfiles();
    String profile = String.join(",", profiles);
    response.setProfile(profile);

    return response;
  }

}
