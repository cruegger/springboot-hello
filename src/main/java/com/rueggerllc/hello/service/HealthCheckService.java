package com.rueggerllc.hello.service;

import com.rueggerllc.hello.model.HealthCheckResponse;

public interface HealthCheckService {
  HealthCheckResponse execute();

}
