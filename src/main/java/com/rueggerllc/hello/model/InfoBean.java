package com.rueggerllc.hello.model;

import lombok.Data;

@Data
public class InfoBean {
    private String dbServer;
    private String dbName;
    private String dbPort;
    private String foo;
}
