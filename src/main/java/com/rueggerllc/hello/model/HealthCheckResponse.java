package com.rueggerllc.hello.model;

import lombok.Data;

@Data
public class HealthCheckResponse {
    private String applicationName;

    private String buildTimestamp;

    private String version;

    private String message;

    private String profile;
}
