package com.rueggerllc.hello.model;

import lombok.Data;

@Data
public class HelloBean {
  private String applicationName;
  private String version;
  private String buildTimestamp;
  private String environments;
  private String message;
  private String newField;
  private String hostName;
  private String dbServer;
}
