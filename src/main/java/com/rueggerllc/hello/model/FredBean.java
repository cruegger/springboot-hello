package com.rueggerllc.hello.model;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FredBean {

  public String execute(String parm) {
    log.info("FredBean.execute {}", parm);
    return "FredBean parm=" + parm;
  }
}
