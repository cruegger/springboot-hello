package com.rueggerllc.hello.model;

import lombok.Data;

@Data
public class HelloResponse {
  private String version;
  private String message;
  private String dbServer;
}
