package com.rueggerllc.hello.model;

import lombok.Data;

@Data
public class HelloRequest {
  private String name;
  private int age;
}
