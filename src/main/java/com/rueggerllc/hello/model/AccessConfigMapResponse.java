package com.rueggerllc.hello.model;

import lombok.Data;

@Data
public class AccessConfigMapResponse {
    private InfoBean infoBean;
}
