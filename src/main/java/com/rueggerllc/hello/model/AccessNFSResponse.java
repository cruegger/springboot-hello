package com.rueggerllc.hello.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class AccessNFSResponse {
    private int code;
    private String msg;

    private List<String> lines = new ArrayList<>();
}
