package com.rueggerllc.hello.config;

import com.rueggerllc.hello.model.FredBean;
import com.rueggerllc.hello.model.InfoBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
@Slf4j
public class HelloConfig {

  @Value("${appconfig.db.server}")
  private String databaseServer;

  @Value("${appconfig.db.name}")
  private String databaseName;

  @Value("${appconfig.db.port}")
  private String databasePort;

  @Value("${appconfig.foo}")
  private String foo;

  @Bean
  FredBean getFred() {
    return new FredBean();
  }

  @Bean
  InfoBean infoBean() {
    InfoBean infoBean = new InfoBean();
    infoBean.setDbServer(databaseServer);
    infoBean.setDbName(databaseName);
    infoBean.setDbPort(databasePort);
    infoBean.setFoo(foo);
    return infoBean;
  }

  @PostConstruct
  public void postConstruct() {
    log.info("============= springboot-hello HelloConfig Initialized ==============");
    log.info("databaseServer={}",databaseServer);
    log.info("databaseName={}",databaseName);
    log.info("databasePort={}",databasePort);
    log.info("foo={}",foo);
  }
}
